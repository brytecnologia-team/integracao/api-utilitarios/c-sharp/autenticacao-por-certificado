﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace Autenticação_por_Certificado
{
    internal class Program
    {
        static string url = "https://hub2.bry.com.br/api/authentication-service/v1/verify-challenge";
        //https://hub2.hom.bry.com.br/api/authentication-service/v1/verify-challenge

        static string caminhoCertificado = "./caminho/para/o/certificado/";
        static string senhaCertificado = "senha do certificado";
        static string token = "insert-a-valid-token";
        
        static string mode = "BASIC";
        static string hashAlgorithm = "SHA1";

        static X509Certificate2 certificate = null;

        //Gera uma String aleatória em base64 para ser assinada (Desafio)
        protected static String gerarDesafio()
        {
            var random = new Random();
            var characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var charactersLength = characters.Length;
            var desafio = "";
            for (int i = 0; i < 64; i++)
            {
                desafio += characters[random.Next(charactersLength)];
            }

            return desafio;
        }

        //Assina com certificado armazenado em disco e retorna o valor da assinatura como uma string base64
        protected static String criptografaConteudo(String conteudo)
        {
            Console.WriteLine("================ Inicializando assinatura ================");
            Console.WriteLine();

            // Pega a chave privada do certificado
            RSA privKeySign = certificate.GetRSAPrivateKey();

            // Decodifica o base64 que está na variável "conteudo"
            byte[] data = Convert.FromBase64String(conteudo);

            // Define o algoritmo de HASH que será utilizado e criptografa o conteudo
            HashAlgorithmName hashAlgorithmStruct;
            HashAlgorithm hasher = null;
            switch (hashAlgorithm)
            {
                case "SHA1":
                    hashAlgorithmStruct = HashAlgorithmName.SHA1;
                    hasher = new SHA1Managed();
                    break;
                case "SHA256":
                    hashAlgorithmStruct = HashAlgorithmName.SHA256;
                    hasher = new SHA256Managed();
                    break;
                case "SHA512":
                    hashAlgorithmStruct = HashAlgorithmName.SHA512;
                    hasher = new SHA512Managed();
                    break;
                default:
                    Console.WriteLine("Algoritmo de HASH inválido. Valores aceitos: 'SHA1, SHA256, SHA512'");
                    Environment.Exit(0);
                    break;
            }
            data = hasher.ComputeHash(data);

            // Cifra/Assina o conteúdo com a chave privada do certificado
            byte[] signature = privKeySign.SignHash(data, hashAlgorithmStruct, RSASignaturePadding.Pkcs1);

            // Codifica em Base64 o conteúdo retornado da cifragem dos dados
            String cifrado = Convert.ToBase64String(signature, Base64FormattingOptions.None);
            Console.WriteLine("Valor da assinatura em Base64: " + cifrado);
            Console.WriteLine();

            return cifrado;
        }

        // Carrega o certificado PKCS12 Armazenado em disco
        public static void carregaCertificado()
        {
            certificate = new X509Certificate2(caminhoCertificado, senhaCertificado, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.PersistKeySet);
        }

        // Calcula a chave pública do certificado para enviar junto a requisição
        public static String getCertificado()
        {
            byte[] certBytes = certificate.RawData;

            return Convert.ToBase64String(certBytes);
        }

        // Verifica desafio
        public static Boolean verificarDesafio(String certificadoTratado, String desafio, String cifrado)
        {
            Console.WriteLine("============= Inicializando Verificação de Desafio no BRy HUB =============\n");
            Console.WriteLine("Desafio: " + desafio);
            Console.WriteLine("Algoritmo Hash: " + hashAlgorithm);

            // Json com os dados da requisição
            String json = "{  \"certificate\"         : \"" + certificadoTratado + "\" ," +
                            "   \"mode\"                : \"" + mode + "\" ," +
                            "   \"hashAlgorithm\"       : \"" + hashAlgorithm + "\" ," +
                            "   \"challenge\"           : \"" + desafio + "\" ," +
                            "   \"challengeSignature\"  : \"" + cifrado + "\" }";

            // Realiza o envio da requisição
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.ContentType = "application/json";
            webRequest.Method = "POST";
            webRequest.Headers["Authorization"] = token;
            using (var streamWriter = new StreamWriter(webRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }
            try
            {
                var httpResponse = (HttpWebResponse)webRequest.GetResponse();
                // Se código é igual a 200
                Console.WriteLine("\nDesafio verificado com sucesso.");
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    dynamic dynamicResult = JsonConvert.DeserializeObject(result);

                    String statusCertificado = dynamicResult.certificateReport.status.status;
                    String statusAssinatura = dynamicResult.signatureVerified;

                    Console.WriteLine("O certificado do assinante está com status: " + statusCertificado);
                    Console.WriteLine("A assinatura do desafio esta com valor: " + statusAssinatura);
                    Console.WriteLine();

                    if (statusCertificado.Equals("VALID") && statusAssinatura.Equals("true"))
                    {
                        return true;
                    }
                }
            }
            catch (WebException e)
            {
                using (var stream = e.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    Console.WriteLine(reader.ReadToEnd());
                }
            }

            return false;
        }

        static void Main(string[] args)
        {
            //Passo 1:
            //Gera o desafio que será assinado pelo certificado. Este passo deve ser realizado pelo servidor de autenticação.
            String desafio = gerarDesafio();

            //Passo 2:
            //Carrega a chave privada e o conteudo do certificado digital
            carregaCertificado();
            String certificadoTratato = getCertificado();

            //Passo 3:
            //Assina o desafio utilizando o certificado em disco
            String cifrado = criptografaConteudo(desafio);

            //Passo 4:
            //Realiza a requisição para o HUB para validação da assinatura e certificado, verificando se o desafio foi aceito. Este passo deve ser realizado pelo servidor de autenticação.
            if (verificarDesafio(certificadoTratato, desafio, cifrado))
            {
                Console.WriteLine("Usuário autenticado com sucesso!\n");

            }
            else
            {
                Console.WriteLine("Usuário não pode ser autenticado.\n");
            }
        }
    }
}
